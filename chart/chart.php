<?php

function ensureDaySet($day) {
    global $days;
    if (!isset($days[$day])) {
        $days[$day] = [
            'count' => 0,
            'hours' => array_fill_keys(range(0, 23), 0)
        ];
    }
}

function getDayString($date) {
    return "{$date['year']}-{$date['mon']}-{$date['mday']}";
}

date_default_timezone_set('Europe/Moscow');

$db = new SQLite3(__DIR__.'/../rusin.db');
$days = [];

// by days
foreach (['comments', 'posts'] as $table) {
    $q = $db->query("SELECT date FROM $table");
    while ($row = $q->fetchArray()) {
        $date = getdate((int)$row['date']);
        $ds = getDayString($date);
        ensureDaySet($ds);

        $days[$ds]['count']++;
    }
}

// by hours
foreach (['comments', 'posts'] as $table) {
    $q = $db->query("SELECT date FROM {$table}_versions");
    while ($row = $q->fetchArray()) {
        $date = getdate((int)$row['date']);
        $ds = getDayString($date);
        ensureDaySet($ds);

        $days[$ds]['hours'][$date['hours']]++;
    }
}

header('Content-Type: text/html; charset=utf-8');
include __DIR__.'/tpl.html';
