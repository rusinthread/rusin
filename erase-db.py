#!/usr/bin/env python2.7
import sqlite3
import os

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))

if __name__ == '__main__':
    conn = sqlite3.connect(os.path.join(SCRIPT_DIR, 'rusin.db'))
    c = conn.cursor()

    for t in ('posts', 'posts_versions', 'comments', 'comments_versions'):
        c.execute('DELETE FROM %s' % t)
    
    conn.commit()

    c.close()
    conn.close()

    print 'all data deleted'
