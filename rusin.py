import time, traceback
from datetime import datetime
from sched import scheduler
from threading import Thread

import vk, util
from util import split_array, serialize, unserialize, create_db_conn

VK_ID = 32595309

def init():
    conn = create_db_conn()

    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS posts (
        post_id INTEGER PRIMARY KEY,
        date INTEGER,
        version INTEGER
    )''')
    c.execute('CREATE INDEX IF NOT EXISTS post_id_idx ON posts (post_id)')
    
    c.execute('''CREATE TABLE IF NOT EXISTS posts_versions (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        post_id INTEGER,
        date INTEGER,
        text TEXT,
        attachments TEXT
    )''')
    c.execute('CREATE INDEX IF NOT EXISTS post_id_idx ON posts_versions (post_id)')

    c.execute('''CREATE TABLE IF NOT EXISTS comments (
        comment_id INTEGER PRIMARY KEY,
        post_id INTEGER,
        user_id INTEGER,
        date INTEGER,
        version INTEGER
    )''')

    c.execute('''CREATE TABLE IF NOT EXISTS comments_versions (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        comment_id INTEGER,
        date INTEGER,
        text TEXT,
        attachments TEXT
    )''')
    c.execute('CREATE INDEX IF NOT EXISTS comment_id_idx ON comments_versions (comment_id)')

    conn.commit()

    c.close()
    conn.close()

def fetch_posts(conn):
    c = conn.cursor()

    response = vk.api('wall.get', {
        'owner_id': VK_ID,
        'filter': 'owner',
        'offset': 0,
        'count': 100,
        'photo_sizes': 1
    })

    posts_id = map(lambda i: i['id'], response['items'])
    posts = {item['id']: item for i, item in enumerate(response['items'])}

    c.execute('''SELECT posts.post_id post_id, text, attachments, version
        FROM posts 
        LEFT JOIN posts_versions
            ON posts_versions.id=posts.version 
        WHERE posts.post_id IN (%s)''' % ','.join(map(lambda x: str(x), posts_id)))
    posts_in_db = {post['post_id']: post for post in c.fetchall()}
    
    for post_id, post  in posts.iteritems():
        if not post_id in posts_in_db:
            add_post(conn, post)
            print 'post %d added' % post_id
        else:
            dbpost = posts_in_db[post_id]
            if dbpost['text'] != post['text'] or (('attachments' in post) and (dbpost['attachments'] != '') and (not _compare_attachments(unserialize(dbpost['attachments']), post['attachments']))):
                edit_post(conn, post_id, post)
                print 'post %d edited' % post_id
    c.close()

    return posts_id

def fetch_comments(conn, posts_id):
    c = conn.cursor()

    execute_code = 'var c = {};'
    for post_id in posts_id:
        execute_code += 'c.comments%d = API.wall.getComments({ owner_id: %d, post_id: %d, offset: 0, count: 100, sort: "asc", preview_length: 0, photo_sizes: 1 });' % (post_id, VK_ID, post_id)
    execute_code += 'return c;'
    
    response = vk.api('execute', {
        'code': execute_code
    })

    for key, data in response.iteritems():
        post_id = int(key.replace('comments', ''))
        if data['count'] == 0:
            continue

        comments_id = map(lambda i: i['id'], data['items'])
        comments = {item['id']: item for i, item in enumerate(data['items'])}

        c.execute('''SELECT comments.comment_id comment_id, text, attachments, version
            FROM comments 
            LEFT JOIN comments_versions
                ON comments_versions.id=comments.version 
            WHERE comments.comment_id IN (%s)''' % ','.join(map(lambda x: str(x), comments_id)))
        comments_in_db = {comment['comment_id']: comment for comment in c.fetchall()}
        
        for comment_id, comment in comments.iteritems():
            if not comment_id in comments_in_db:
                add_comment(conn, post_id, comment)
                print 'comment %d to post %d added' % (comment_id, post_id)
            else:
                dbcomment = comments_in_db[comment_id]
                if dbcomment['text'] != comment['text'] or (('attachments' in comment) and (dbcomment['attachments'] != '') and (not _compare_attachments(unserialize(dbcomment['attachments']), comment['attachments']))):
                    edit_comment(conn, comment_id, comment)
                    print 'comment %d edited' % comment_id

    c.close()

def add_post(conn, post):
    c = conn.cursor()
    attachments = serialize(post['attachments']) if 'attachments' in post else ''
    
    c.execute('INSERT INTO posts_versions (post_id, date, text, attachments) VALUES (?, ?, ?, ?)', (post['id'], post['date'], post['text'], attachments))
    version_id = c.lastrowid

    c.execute('INSERT INTO posts (post_id, date, version) VALUES (?, ?, ?)', (post['id'], post['date'], version_id))
    conn.commit()

    c.close()

def edit_post(conn, post_id, post):
    c = conn.cursor()
    attachments = serialize(post['attachments']) if 'attachments' in post else ''

    date = int(time.time())

    c.execute('INSERT INTO posts_versions (post_id, date, text, attachments) VALUES (?, ?, ?, ?)', (post_id, date, post['text'], attachments))
    new_version_id = c.lastrowid

    c.execute('UPDATE posts SET version=? WHERE post_id=?', (new_version_id, post_id))
    conn.commit()

    c.close()

def add_comment(conn, post_id, comment):
    c = conn.cursor()
    attachments = serialize(comment['attachments']) if 'attachments' in comment else ''

    c.execute('INSERT INTO comments_versions (comment_id, date, text, attachments) VALUES (?, ?, ?, ?)', (comment['id'], comment['date'], comment['text'], attachments))
    version_id = c.lastrowid

    c.execute('INSERT INTO comments (post_id, comment_id, user_id, date, version) VALUES (?, ?, ?, ?, ?)', (post_id, comment['id'], comment['from_id'], comment['date'], version_id))
    conn.commit()

    c.close()

def edit_comment(conn, comment_id, comment):
    c = conn.cursor()
    attachments = serialize(comment['attachments']) if 'attachments' in comment else ''

    date = int(time.time())

    c.execute('INSERT INTO comments_versions (comment_id, date, text, attachments) VALUES (?, ?, ?, ?)', (comment_id, date, comment['text'], attachments))
    new_version_id = c.lastrowid

    c.execute('UPDATE comments SET version=? WHERE comment_id=?', (new_version_id, comment_id))
    conn.commit()

    c.close()

def _attachments_fingerprint(attachments):
    data = []

    for a in attachments:
        t = a['type']
        d = a[t]

        if t in ('photo', 'posted_photo', 'video', 'audio', 'graffiti', 'note', 'album', 'market_album', 'market', 'poll'):
            a_id = str(d['owner_id']) + '_' + str(d['id'])
        elif t == 'page':
            a_id = str(d['group_id']) + '_' + str(d['id'])
        elif t == 'app':
            a_id = str(d['id'])
        elif t == 'link':
            a_id = d['url']

        data.append('%s|%s' % (t, a_id))

    return ';'.join(data)

def _compare_attachments(a1, a2):
    fp1 = _attachments_fingerprint(a1)
    fp2 = _attachments_fingerprint(a2)
    #print '%s %s' % (fp1, fp2)
    return fp1 == fp2

def _get_post_attachments(attachments):
    return unserialize(attachments) if attachments else []

def get_posts(conn, offset, count):
    c = conn.cursor()

    c.execute('''SELECT 
            posts.post_id id,
            posts.date original_date,
            posts_versions.date edit_date,
            text,
            attachments
        FROM posts
        LEFT JOIN posts_versions
            ON posts_versions.id=posts.version
        ORDER BY posts.post_id DESC
        LIMIT ?, ?''', (offset, count))

    posts = []
    rows = c.fetchall()

    for row in rows:
        c.execute('''SELECT
                comments.comment_id id,
                user_id,
                comments.date original_date,
                comments_versions.date edit_date,
                text, 
                attachments
            FROM comments
            LEFT JOIN comments_versions
                ON comments_versions.id=comments.version
            WHERE comments.post_id=?
            ORDER BY comments.comment_id''', (row['id'],))
        
        comments = []
        for c_row in c.fetchall():
            comment_edited = c_row['edit_date'] != c_row['original_date']
            comment = {
                'id': c_row['id'],
                'user': user_fetcher.get(c_row['user_id']),
                'original_date': c_row['original_date'],
                'edit_date': c_row['edit_date'],
                'text': c_row['text'],
                'attachments': _get_post_attachments(c_row['attachments']),
                'edited': comment_edited
            }
            comments.append(comment)

        post_edited = row['edit_date'] != row['original_date']
        post = {
            'id': row['id'],
            'user': user_fetcher.get(VK_ID),
            'original_date': row['original_date'],
            'edit_date': row['edit_date'],
            'text': row['text'],
            'attachments': _get_post_attachments(row['attachments']),
            'comments': comments,
            'edited': post_edited
        }
        posts.append(post)

    c.close()
    return posts

def get_post_versions(conn, post_id):
    c = conn.cursor()

    c.execute('''SELECT date, text, attachments FROM posts_versions WHERE post_id=? ORDER BY id''', (post_id,))
    rows = c.fetchall()
    
    def f(row):
        row['attachments'] = _get_post_attachments(row['attachments'])
        return row
    rows = map(f, rows)

    c.close()
    return rows

def get_comment_versions(conn, comment_id):
    c = conn.cursor()

    c.execute('''SELECT date, text, attachments FROM comments_versions WHERE comment_id=? ORDER BY id''', (comment_id,))
    rows = c.fetchall()
    
    def f(row):
        row['attachments'] = _get_post_attachments(row['attachments'])
        return row
    rows = map(f, rows)

    c.close()
    return rows

def get_stats(conn):
    c = conn.cursor()
    return {
        'comments': int(c.execute('SELECT COUNT(*) c FROM comments').fetchone()['c']),
        'posts': int(c.execute('SELECT COUNT(*) c FROM posts').fetchone()['c'])
    }
    c.close()

class UserFetcher:
    TIMEOUT = 3600

    def __init__(self):
        self.cache = {}

    def get(self, user_id):
        #print 'UserFetcher.get %s' % str(user_id)
        if (user_id not in self.cache) or (self.cache[user_id]['time'] < time.time() - UserFetcher.TIMEOUT):
            response = vk.api('users.get', {
                'user_ids': user_id,
                'fields': 'name,photo_50'
            })
            self.cache[user_id] = {'data': response[0], 'time': time.time()}

        return self.cache[user_id]['data']

class RusinWorker(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.daemon = True

    def run(self):
        self.conn = create_db_conn()
        self.cursor = self.conn.cursor()

        self.scheduler = scheduler(time.time, time.sleep)
        self.scheduler.enter(0, 1, self.poll, (self.scheduler,))
        self.scheduler.run()

    def poll(self, s):
        #print 'poll() %s' % datetime.now().strftime('%d.%m.%Y %H:%M:%S')

        try:
            posts_id = fetch_posts(self.conn)
            posts_id_parts = split_array(posts_id, 20)
            for comments_posts_id in posts_id_parts:
                fetch_comments(self.conn, comments_posts_id)

        except Exception, e:
            traceback.print_exc()

        #print 'Done'

        self.scheduler.enter(60, 1, self.poll, (self.scheduler,))

user_fetcher = UserFetcher()
