#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-,
import web, math
from pprint import pprint

import tpl, config, rusin
from util import create_db_conn

PER_PAGE = 30

class index:
    def GET(self):
        i = web.input(page=1)
        page = int(i.page)

        with create_db_conn() as conn:
            stats = rusin.get_stats(conn)
            
            pages = int(math.ceil(float(stats['posts']) / PER_PAGE))

            if page < 1: page = 1
            if page > pages: page = pages

            offset = (page-1) * PER_PAGE

            posts = rusin.get_posts(conn, offset, PER_PAGE)

        posts = ''.join(map(lambda p: tpl.render_post(p), posts))
        stats = tpl.render_stats(stats)

        pagenav = tpl.render_pagenav(page, pages, '/?page=%d') if pages > 1 else ''
        
        return tpl.page('rusin logger for /sn/', tpl.html('index', posts, stats, pagenav))

class versions:
    def GET(self, *args):
        obj_type = args[0]
        obj_id = int(args[1])

        with create_db_conn() as conn:
            versions = rusin.get_post_versions(conn, obj_id) if obj_type == 'post' else rusin.get_comment_versions(conn, obj_id)
        
        return tpl.page('%s %d versions' % (obj_type, obj_id), tpl.render_post_versions(versions))

class MyApplication(web.application):
    def run(self, port=8080, host='127.0.0.1', *middleware):
        func = self.wsgifunc(*middleware)
        return web.httpserver.runsimple(func, (host, port))

urls = (
    '/', 'index',
    '/(post|comment)/(\d+)', 'versions',
)

if __name__ == '__main__':
    rusin.init()

    rusin_worker = rusin.RusinWorker()
    rusin_worker.start()

    web.config.debug = config.debug

    app = MyApplication(urls, globals())
    app.run(host=config.host, port=config.port)
