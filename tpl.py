# -*- coding: utf-8 -*-
import web

import config, rusin
from util import vk_user_link, num_label, pretty_date, get_duration

_render = web.template.render('templates/', cache=not config.debug, globals={'num_label': num_label})

def html(name, *args, **kwargs):
    return unicode(getattr(_render, name)(*args, **kwargs))

def page(title, content):
    return _render.layout(title, content)

def render_post(post):
    user = post['user']
    comments_count = len(post['comments'])
    data = {
        'id': post['id'],
        'user_name': user['first_name'] + ' ' + user['last_name'],
        'user_link': vk_user_link(user['id']),
        'user_image': user['photo_50'],
        'text': post['text'],
        'date': render_post_date(post['original_date'], post_id=post['id'], edited=post['edited'], edit_date=post['edit_date']),
        'comments_count': comments_count
    }

    comments = ''
    if comments_count:
        for comment in post['comments']:
            comments += render_comment(comment, post['id'])

    attachments = ''
    if post['attachments']:
        attachments = render_attachments(post['attachments'])

    return html('post', data, comments, attachments)

def render_post_date(original_date, post_id=0, comment_id=0, edited=False, edit_date=0):
    post_type = 'comment' if comment_id != 0 else 'post'
    post_url = 'https://vk.com/wall32595309_' + str(post_id)
    if comment_id != 0:
        post_url += '?reply=' + str(comment_id)
    date = '<a class="post_date" href="'+post_url+'">'+pretty_date(original_date)+'</a>'
    if edited:
      date += u', изменено %s, <a class="history_link" href="/%s/%d">смотреть историю</a>' % (pretty_date(edit_date), post_type, post_id if post_type == 'post' else comment_id)
    return date

def render_comment(comment, post_id):
    user = comment['user']
    data = {
        'id': comment['id'],
        'user_name': user['first_name'] + ' ' + user['last_name'],
        'user_link': vk_user_link(user['id']),
        'user_image': user['photo_50'],
        'text': comment['text'],
        'date': render_post_date(comment['original_date'], post_id=post_id, comment_id=comment['id'], edited=comment['edited'], edit_date=comment['edit_date'])
    }

    attachments = ''
    if comment['attachments']:
        attachments = render_attachments(comment['attachments'])

    return html('comment', data, attachments)

def render_attachments(attachments):
    output = ''
    for a in attachments:
        if a['type'] == 'photo':
            photo = a['photo']
            latest_max_w = 0
            for size in photo['sizes']:
                if size['type'] == 'm':
                    photo_preview = size
                else:
                    if size['width'] > 0 and size['width'] > latest_max_w:
                        photo_full = size
                        latest_max_w = size['width']

            output += html('attach_photo', photo_preview['src'], photo_full['src'])

        elif a['type'] == 'video':
            video = a['video']
            link = 'https://vk.com/video' + str(video['owner_id']) + '_' + str(video['id'])
            output += html('attach_video', video['photo_320'], video['title'], link, get_duration(video['duration']))
        
        else:
            output += html('attach_unknown', a['type'])

    return output

def render_post_versions(versions):
    return ''.join(map(lambda v: render_post_version(v), versions))

def render_post_version(v):
    attachments = ''
    if v['attachments']:
        attachments = render_attachments(v['attachments'])

    return html('post_version', v['text'], pretty_date(v['date']), attachments)

def render_stats(stats):
    return html('stats', stats['posts'], stats['comments'])

def render_pagenav(page, pages, url):
    data = ''
    i = 1
    while i <= pages:
        if i == page:
            data += '<b>'+str(i)+'</b>'
        else:
            data += '<a href="'+(url % (i,))+'">'+str(i)+'</a>'
        i += 1
    return html('pagenav', data)
