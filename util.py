# -*- coding: utf-8 -*-

import sqlite3, os, json, time
from datetime import datetime
from math import floor

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
DB_PATH = os.path.join(SCRIPT_DIR, 'rusin.db')

def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

def create_db_conn():
    conn = sqlite3.connect(DB_PATH)
    conn.row_factory = dict_factory
    return conn

def split_array(a, n):
    return [a[i:i+n] for i in range(0, len(a), n)]

def serialize(val):
    return json.dumps(val, separators=(',', ':'))

def unserialize(val):
    return json.loads(val)

def num_label(num, *args):
    n = num % 100
    if n > 19:
        n %= 10

    if n == 1:
        return args[0]
    elif n >= 2 and n <= 4:
        return args[1]
    else:
        return args[2]

def pretty_date(timestamp):
    date = datetime.fromtimestamp(int(timestamp))
    months = (
        u'янв', u'фев', u'мар', u'апр', u'май', u'июн', u'июл', u'авг', u'сен', u'окт', u'ноя', u'дек'
    )
    return u'%d %s в %s' % (date.day, months[date.month-1], date.strftime('%H:%M'))

def vk_user_link(user_id):
    return 'https://vk.com/id%d' % user_id

def get_duration(s):
    m = floor(s / 60)
    s %= 60
    return '%02d:%02d' % (int(m), int(s))

class MeasureTime:
    def __init__(self, name):
        self.name = name
        self.start = time.clock()

    def end(self):
        print 'time of %s: %f' % (self.name, time.clock() - self.start)
