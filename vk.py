import requests
import time
import config

API_VERSION = '5.53'

class VKException(Exception):
    def __init__(self, error):
        super(VKException, self).__init__()
        self.code = error['error_code']
        self.message = error['error_msg']
        self.request_params = error['request_params']

    def __str__(self):
        return '[%d] %s' % (self.code, self.message)

def api(method, params):
    tries = 5
    while tries > 0:
        url = 'https://api.vk.com/method/' + method
        params['v'] = API_VERSION
        params['access_token'] = config.api_token

        r = requests.post(url, data=params)
        response = r.json()
        
        if 'error' in response:
            error = response['error']
            code = error['error_code']
            if code != 6 or tries == 0:
                error = VKException(response['error'])
                if config.tg_chat_id > 0 and config.tg_bot_token != '':
                    report_error_tg(error)
                raise error
            else:
                tries -= 1
                time.sleep(1)
        else:
            break
    
    return response['response']

def report_error_tg(vk_error):
    url = 'https://api.telegram.org/bot'+config.tg_bot_token+'/sendMessage'
    try:
        requests.post(url, data={
            'chat_id': config.tg_chat_id,
            'text': 'rusin error: ' + unicode(vk_error)
        })
    except Exception, e:
        print e
